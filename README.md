# Using CloudStack API

This project demonstrates how to directly use the
[CloudStack API](https://cloudstack.apache.org/api/apidocs-4.16/)
to create, start and stop a virtual machine hosted in ci.inria.fr.
The pipeline specified in [`.gitlab-ci.yml`](.gitlab-ci.yml)
checks whether the virtual machine exists and creates it if needed,
checks whether it is running and starts it if needed,
executes a build job,
and finally stops the virtual machine.

## Prerequisites

In addition to CI/CD features and shared runners (see the gitlabci_gallery
[Prerequisites section](https://gitlab.inria.fr/gitlabci_gallery#prerequisites)
), this project has the following requirement:

- Cloudstack API and secret keys should be added as variables
  `CLOUDSTACK_API_KEY` and `CLOUDSTACK_SECRET_KEY` of type
  **Variable** in CI/CD settings.
  See the
  [details on how to set a CI/CD variable](https://gitlab.inria.fr/gitlabci_gallery#cicd-variables).
  To get the Cloudstack API and secret keys:
  - Go to https://sesi-cloud-ctl1.inria.fr/
  - Login as the dedicated user, with `ci/project-name` as domain.
  - On the left sidebar, go to **Accounts**.
  - Select **admins**, then **View Users**.
  - Select the dedicated user.
  - Copy the fields **API Key** and **Secret key**.

- GitLab runner registration token should be added as a variable
  `REGISTRATION_TOKEN` of type **Variable** in CI/CD settings.
  This token will allow the virtual machine to
  register itself as GitLab runner to execute jobs in the
  project pipeline.
  Note that this variable is only required if you rely on the
  pipeline to create and initialize the virtual machine: there is
  no need for this variable if the virtual machine preexists and
  is already registered as a GitLab runner.
  To get the registration token:
  - On the left sidebar in the GitLab interface, go to **Settings** → **CI/CD**.
  - Expand **Runners**.
  - In the **Specific runners** section,
    you will find it after the label **And this registration token:**.
    There is a button **Copy token** just after the token to copy it
    to the clipboard.

## The pipeline specification file [`.gitlab-ci.yml`](.gitlab-ci.yml)

The pipeline specification file [`.gitlab-ci.yml`](.gitlab-ci.yml)
uses `workflow:rules:if` to be enabled only if `$CLOUDSTACK_SECRET_KEY`
is available: this variable contains a secret and should be protected,
which makes it only available on protected branches.
The pipeline is not executed when `$CLOUDSTACK_SECRET_KEY`
is unavailable, _i.e._ on non-protected branches.

```yaml
workflow:
  rules:
    - if: $CLOUDSTACK_SECRET_KEY
```

The following job `prepare` runs on [`.pre` stage](https://docs.gitlab.com/ee/ci/yaml/#stage-pre).
The job uses a shared runner with the docker image
`registry.gitlab.inria.fr/inria-ci/docker/python3-gitlabci`: this docker container provides Python 3.11 and packages to access CloudStack and GitLab APIs
(see the [specification](https://gitlab.inria.fr/inria-ci/docker/-/tree/main/python3-gitlabci)).
The job runs the script [`prepare-vm.py`](prepare-vm.py) with the argument
`start`, to create the virtual machine if it does not exist, and starts it if
it is suspended.
The name and the specification of the virtual machines are hard-coded in the
script.
As the other jobs of the pipeline, the job is linked to the
[`resource_group`](https://docs.gitlab.com/ee/ci/resource_groups/index.html)
`cloudstack-vm` to prevent concurrent pipelines to be run
in parallel (so that no pipeline will stop the virtual machine while another
pipeline runs job on it).

```yaml
prepare:
  stage: .pre
  tags:
    - linux
    - small
  image: "registry.gitlab.inria.fr/inria-ci/docker/python3-cloudstack"
  script:
    - python3 prepare-vm.py start
  resource_group: cloudstack-vm
```

The `build` job runs on the virtual machine initialized above, using the tags
`cloudstack` and `docker` to target it.

```yaml
build:
  tags:
    - cloudstack
    - docker
  script:
    - apk add gcc musl-dev
    - gcc -o hello_world hello_world.c
  resource_group: cloudstack-vm
```

The `stop vm` job runs on
[`.post` stage](https://docs.gitlab.com/ee/ci/yaml/#stage-post).
The job runs the script [`prepare-vm.py`](prepare-vm.py) with the argument
`stop`, to suspend the virtual machine.
The key [`when: always`](https://docs.gitlab.com/ee/ci/yaml/#when)
makes the job run even if previous stages have failed, to suspend the virtual
machine in every case.

```
stop vm:
  stage: .post
  tags:
    - linux
    - small
  image: "registry.gitlab.inria.fr/inria-ci/docker/python3-cloudstack"
  script:
    - python3 prepare-vm.py stop
  resource_group: cloudstack-vm
  when: always
```

## The Python script [`prepare-vm.py`](prepare-vm.py)

This Python script demonstrates how to use
[CloudStack API](https://cloudstack.apache.org/api/apidocs-4.16/).
The script relies on the environment variables
`CLOUDSTACK_API_KEY`, `CLOUDSTACK_SECRET_KEY`
and `REGISTRATION_TOKEN` (only used on virtual machine creation).

The name of the virtual machine is hard-coded in the variable
`vm_name` in function `main`, and the specification of
the virtual machine is hard-coded in `create_vm`.
To initialize the virtual machine, this function
uses [`cloud-init`](https://cloud-init.io/) to
install `gitlab-runner` and `docker` on first boot,
and register the runner on GitLab
(by using `REGISTRATION_TOKEN`).
The virtual machine uses the custom service offering,
with CPU number and memory (in GB) specified in
the `details` field.
